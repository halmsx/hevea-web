<?php

// header('Access-Control-Allow-Origin: *');

require_once 'db.php';

$output = '';
$post = $_POST;

if (isset($post['refno'])) {
  // need to cycle through form fields

  $result = $database->select("article", '*');

  $error = $database->error();

  if ($error[1] == NULL) {
    $output = array('error' => '0', 'error_message' => 'Rekod berjaya dimasukkan.');
  } else {
    $output = array('error' => $error[1], 'error_message' => '' + $error[2]);
  }
  $output = array("error" => '1', 'error_message' => 'Tiada data diberikan.');
}

$output = json_encode($post);
print ($output);
