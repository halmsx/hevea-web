<?php
header('Access-Control-Allow-Origin: *');

require_once 'db.php';

$output = '';

$result = $database->update("article", ["flag" => 1]);

$error = $database->error();

if ($error[1] == NULL) {
	$output = array('error' => '0', 'error_message' => 'Rekod berjaya dimasukkan.');
} else {
	$output = array('error' => $error[1], 'error_message' => '' + $error[2]);
}

$output = json_encode($result);

print ($output);
