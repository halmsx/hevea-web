<?php
header('Access-Control-Allow-Origin: *');

$ref = 0;

if (isset($_GET['ref']))
	$ref = $_GET['ref'];
else
	$ref = $_POST['ref'];

if ($ref != 0) {
	$directory = '/tmp/hevea/uploads/' . $ref;

	$files = scandir($directory);

	$dots = array(".", "..");

	$result = array_diff($files, $dots);

	$output = json_encode($result);

	// print_r ($result);
	print ($output);
}

/*
// test data
$dataImg = json_decode($output);

foreach ($dataImg as $key => $value) {
echo '<pre>';
	# code...
	// print_r ($key);
	// print('...');
	print ($directory . "/" . $value);
echo '<br>';
echo '</pre>';
}
*/