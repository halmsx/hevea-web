<?php
header('Access-Control-Allow-Origin: *');

require_once 'db.php';

$output = '';

$post = $_POST;

$result = $database->select("article", "*", ["flag" => 0]);

$error = $database->error();

if ($error[1] == NULL) {
	$output = array('error' => '0', 'error_message' => '.');
} else {
	$output = array('error' => $error[1], 'error_message' => '' + $error[2]);
}

$output = json_encode($result);

print ($output);
