/**
 * Implements hook_menu().
 */
function heveaMain_menu() {
  try {
    var items = {};
    items['hevea_main'] = {
      title: 'You are online.',
      page_callback: 'heveaMain_page',
      options:{
        transition:'flip'
      }
    };
    return items;
  }
  catch (error) { console.log('hevea_menu - ' + error); }
}

function heveaMain_page () {
  try {
    var content = {};
    content['my_intro_text'] = {
      markup: '<h2 style="text-align: center">Welcome to HeveaWatch</h2>' +
      				'<p style="text-align: center">Please login to continue</p>'
    };
    return content;
  }
  catch (error) { console.log('heveaMain_page - ' + error); }
}
