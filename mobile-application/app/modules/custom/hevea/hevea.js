
/**
 * Implements hook_block_info().
 */
function hevea_block_info() {
  try {
    var blocks = {};
    blocks['hevea_footer'] = {
      delta: 'hevea_footer',
      module: 'hevea'
    };
    return blocks;
  }
  catch (error) { console.log('hevea_footer_info - ' + error); }
}


/**
 * Implements hook_block_view().
 */
function hevea_block_view(delta, region) {
  try {
    var content = '';
    if (delta == 'hevea_footer') {
      // Show today's date for the block's content.
      var d = new Date();
      content = '<p class="footer-text">&copy; 2015 HeveaWatch</p>';
    }
    return content;
  }
  catch (error) { console.log('hevea_block_view - ' + error); }
}

/**
 * Implements hook_deviceready().
 *
 * overrides the connection failed page
 */
function hevea_deviceready() {
	try {
		var connection = drupalgap_check_connection();

		if (drupalgap.online) {
			alert('true');
		} else {
			alert('false');
		}
	}
	catch (error) { console.log('hevea_deviceready - ' + error); }
}
