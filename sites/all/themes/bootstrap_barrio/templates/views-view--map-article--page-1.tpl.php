
<script>
(function ($) {
  var layers=[];
  
  function loadScript () {
    console.log(maps);

    layers[0] = new  google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Daerah_NegeriSembilanWGS84.kmz", {preserveViewport: true});
    layers[1] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Mukim_Bandar_N9_GCS_WGS84.kmz", {preserveViewport: true});
    layers[2] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu_Clear_Land.kmz", {preserveViewport: true});
    layers[3] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu_Hutan.kmz", {preserveViewport: true});
    layers[4] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu_Sawit.kmz", {preserveViewport: true});
    // layers[5] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu_Urban.kmz", {preserveViewport: true});
    // layers[6] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu_Getah.kmz", {preserveViewport: true});
    layers[7] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Sempadan_negeriWGS84.kmz", {preserveViewport: true});
    // layers[8] = new google.maps.KmlLayer("http://hevea.boikot.my/hevea1/kml/kmz/Updated_Jelebu.kmz", {preserveViewport: true});
  
  }

    function toggleKML (num) {
      alert(num);
    }

  google.maps.event.addDomListener(window, 'load', loadScript);

})(jQuery);
</script>

<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>


<div class="select-layer">
Sempadan Daerah <input type="checkbox" id="layer_01" onclick="toggleKML(0);"/><br>
Sempadan Mukim <input type="checkbox" id="layer_02" onclick="toggleLayers(1);"/><br>
Jelebu - Clear Land <input type="checkbox" id="layer_03" onclick="toggleLayers(2);"/><br>
Jelebu - Hutan <input type="checkbox" id="layer_03" onclick="toggleLayers(3);"/><br>
Jelebu - Sawit <input type="checkbox" id="layer_03" onclick="toggleLayers(4);"/><br>
<!-- Layer6 <input type="checkbox" id="layer_03" onclick="toggleLayers(5);"/> -->
<!-- Layer7 <input type="checkbox" id="layer_03" onclick="toggleLayers(6);"/> -->
Sempadan Negeri <input type="checkbox" id="layer_03" onclick="toggleLayers(7);"/><br> 
<!-- Layer9 <input type="checkbox" id="layer_03" onclick="toggleLayers(8);"/> -->
</div>
  <div id="status"></div>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
