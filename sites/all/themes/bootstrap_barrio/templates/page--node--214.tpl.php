

<style>
  html, body, #map-canvas { 
    height: 100%; 
    margin: 0; 
    padding: 0;
  }

  #select-layer {
    position: absolute;
    background: #fff;
    border-radius: 5px;
    z-index: 102;

    padding: 20px;
    right: 20px;
    top: 100px;
  }
  #map-canvas {
    z-index: 101;
  }
</style>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/geolocationmarker/src/geolocationmarker-compiled.js"></script>

<script type="text/javascript">

          var map;
          var latlng;

          var mapBounds = new google.maps.LatLngBounds(
                  new google.maps.LatLng(2.390733, 101.702447),
                  new google.maps.LatLng(3.290369, 102.702168));

          var mapMinZoom = 8;
          var mapMaxZoom = 12;


          // landsat map tiler
          var maptiler = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
              var proj = map.getProjection();
              var z2 = Math.pow(2, zoom);
              var tileXSize = 256 / z2;
              var tileYSize = 256 / z2;
              var tileBounds = new google.maps.LatLngBounds(
                      proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                      proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                      );
              var y = coord.y;
              if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom))
                return "map-zoom/" + zoom + "/" + coord.x + "/" + y + ".png";
              else
                return "http://www.maptiler.org/img/none.png";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
          });

          // N9 barat maptile
          var TileN9West = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
              var proj = map.getProjection();
              var z2 = Math.pow(2, zoom);
              var tileXSize = 256 / z2;
              var tileYSize = 256 / z2;
              var tileBounds = new google.maps.LatLngBounds(
                      proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                      proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                      );
              var y = coord.y;
              var x = coord.x >= 0 ? coord.x : z2 + coord.x
              if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom))
                return "kml/baratN9/" + zoom + "/" + x + "/" + y + ".png";
              else
                return "http://www.maptiler.org/img/none.png";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
          });

          // N9 timur maptile
          var TileN9East = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
              var proj = map.getProjection();
              var z2 = Math.pow(2, zoom);
              var tileXSize = 256 / z2;
              var tileYSize = 256 / z2;
              var tileBounds = new google.maps.LatLngBounds(
                      proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                      proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                      );
              var y = coord.y;
              var x = coord.x >= 0 ? coord.x : z2 + coord.x
              if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom))
                return "kml/timurN9/" + zoom + "/" + x + "/" + y + ".png";
              else
                return "http://www.maptiler.org/img/none.png";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
          });

          // end tiles

          function initialize() {
            latlng = new google.maps.LatLng(2.76744, 102.23784);
            // latlng = new google.maps.LatLng(3.1454815, 101.6983001);

            var mapOptions = {
              zoom: 8,
              scrollwheel: false,
              center: latlng,
              streetViewControl: false,
              mapTypeIds: google.maps.MapTypeId.ROADMAP

            }
            map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            /*
             * get geo location
             * 
             * http://google-maps-utility-library-v3.googlecode.com/svn/trunk/geolocationmarker/docs/reference.html
             */

//            var GeoMarker = new GeolocationMarker(map); // simple direct 

            GeoMarker = new GeolocationMarker();
            GeoMarker.setCircleOptions({fillColor: '#808080'});

            google.maps.event.addListenerOnce(GeoMarker, 'position_changed', function () {
              map.setCenter(this.getPosition());
              map.fitBounds(this.getBounds());
//              map.setZoom(mapMaxZoom);
            });

            google.maps.event.addListener(GeoMarker, 'geolocation_error', function (e) {
              alert('There was an error obtaining your position. Message: ' + e.message);
            });

            GeoMarker.setMap(map);
          }

          function mapLandSatTile() {
            var landsat = document.getElementById('landsat');
            
            if (landsat.checked) {
              map.overlayMapTypes.setAt(0, maptiler);
            } else {
              map.overlayMapTypes.setAt(0, null);
            }
          }

          function tileN9West() {
            var landsat = document.getElementById('tileN9West');
            
            if (landsat.checked) {
              map.overlayMapTypes.setAt(0, TileN9West);
            } else {
              map.overlayMapTypes.setAt(0, null);
            }
          }

          function tileN9East() {
            var landsat = document.getElementById('tileN9East');
            
            if (landsat.checked) {
              map.overlayMapTypes.setAt(0, TileN9East);
            } else {
              map.overlayMapTypes.setAt(0, null);
            }
          }

          google.maps.event.addDomListener(window, 'load', initialize);

          /*
           layers switch
           */
          var layers = [];
          var kmlUrl = "http://hevea.boikot.my/hevea1/kml/kmz/";

          //data basic layer
          layers[0] = new google.maps.KmlLayer(kmlUrl + "Negeri-Sempadan.kmz", {preserveViewport: true});
          layers[1] = new google.maps.KmlLayer(kmlUrl + "N9-Daerah.kmz", {preserveViewport: true});
          layers[2] = new google.maps.KmlLayer(kmlUrl + "N9-Mukim.kmz", {preserveViewport: true});

          //data daerah jelebu
          layers[3] = new google.maps.KmlLayer(kmlUrl + "N9_Jelebu_Getah.kmz", {preserveViewport: true});
          layers[4] = new google.maps.KmlLayer(kmlUrl + "N9_Jelebu_Sawit.kmz", {preserveViewport: true});
          layers[5] = new google.maps.KmlLayer(kmlUrl + "N9_Jelebu_Urban.kmz", {preserveViewport: true});
          layers[6] = new google.maps.KmlLayer(kmlUrl + "N9_Jelebu_Clearland.kmz", {preserveViewport: true});
          layers[7] = new google.maps.KmlLayer(kmlUrl + "N9_Jelebu_Belukar_Hutan.kmz", {preserveViewport: true});
          //data daerah jelebu
          layers[200] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetah5Peradong.kml", {preserveViewport: true});
          layers[201] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetah6Peradong.kml", {preserveViewport: true});
          layers[202] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahGelamiLemi.kml", {preserveViewport: true});
          layers[203] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahHuluKelawang.kml", {preserveViewport: true});
          layers[204] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahPertang.kml", {preserveViewport: true});
          layers[205] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahHuluTeriang.kml", {preserveViewport: true});
          layers[206] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahKenaboi.kml", {preserveViewport: true});
          layers[207] = new google.maps.KmlLayer(kmlUrl + "jelebu-getah/DaerahJelebuGetahTeriangHilir.kml", {preserveViewport: true});

          //data daerah seremban
          layers[8] = new google.maps.KmlLayer(kmlUrl + "N9_Seremban_Getah.kmz", {preserveViewport: true});
          layers[9] = new google.maps.KmlLayer(kmlUrl + "N9_Seremban_Sawit.kmz", {preserveViewport: true});
          layers[10] = new google.maps.KmlLayer(kmlUrl + "N9_Seremban_Urban.kmz", {preserveViewport: true});
          layers[11] = new google.maps.KmlLayer(kmlUrl + "N9_Seremban_Clearland.kmz", {preserveViewport: true});
          layers[12] = new google.maps.KmlLayer(kmlUrl + "N9_Seremban_Belukar_Hutan.kmz", {preserveViewport: true});
          //data daerah seremban - getah
          layers[800] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahAmpangan.kml", {preserveViewport: true});
          layers[801] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahBandarSeremban.kml", {preserveViewport: true});
          layers[802] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahLabu.kml", {preserveViewport: true});
          layers[803] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahLenggeng.kmz", {preserveViewport: true});
          layers[804] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahPantai.kml", {preserveViewport: true});
          layers[805] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahRantau.kmz", {preserveViewport: true});
          layers[806] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahRasau.kml", {preserveViewport: true});
          layers[807] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahSeremban.kml", {preserveViewport: true});
          layers[808] = new google.maps.KmlLayer(kmlUrl + "seremban-getah/DaerahSerembanGetahSetul.kml", {preserveViewport: true});


          function toggleLayers(i)
          {

            if (layers[i].getMap() == null) {
              layers[i].setMap(map);
            }
            else {
              layers[i].setMap(null);
            }

            document.getElementById('status').innerHTML = "toggleLayers(" + i + ") [setMap(" + layers[i].getMap() + "] returns status: " + layers[i].getStatus() + "<br>";
          }
</script>

<?php if (!empty($secondary_nav) || !empty($page['top'])): ?>
  <header role="banner">
    <div id="top" class="container">
      <?php print render($secondary_nav); ?>
      <?php print render($page['top']); ?>
    </div>
  </header>
<?php endif; ?>
<header id="navbar" class="<?php print $navbar_classes; ?>" role="banner">
  <div class="navbar-inner">
    <div id="main-navs">
      <div class="container">
        <div class="navbar-header">
          <?php if (!empty($logo)): ?>
            <a class="logo pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>

          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="brand"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
          <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div id="header" class="header">
          <div>
            <?php print render($page['header']); ?>
            <?php if (!empty($page['yamm'])): ?>
              <div class="navbar yamm">
                <div id="navyamm" class="nav-collapse">
                  <ul class="nav navbar-nav">
                    <?php print render($page['yamm']); ?>
                  </ul>
                </div>
              </div>
            <?php endif; ?>
            <?php if (!empty($primary_nav) || !empty($page['navigation'])): ?>
              <div id="main-nav" class="<?php print $collapse; ?>">
                <nav role="navigation">
                  <?php if (!empty($primary_nav)): ?>
                    <?php print render($primary_nav); ?>
                  <?php endif; ?>
                  <?php if (!empty($page['navigation'])): ?>
                    <?php print render($page['navigation']); ?>
                  <?php endif; ?>
                </nav>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div></div>
  </div>
</header>

<div class="main-container container google-map">

  <header role="banner" id="page-header">
  </header> <!-- /#header -->

  <div class="row">

    <section class="<?php print $content_width; ?>">  

      <div id="select-layer" class="select-layer">
        <details>
          <summary>Base Layers <br /></summary>
          <input type="checkbox" id="layer_03" onclick="toggleLayers(0);"/> Sempadan Negeri <br> 
          <input type="checkbox" id="layer_01" onclick="toggleLayers(1);"/> Sempadan Daerah <br>
          <input type="checkbox" id="layer_02" onclick="toggleLayers(2);"/> Sempadan Mukim <br>
          <input type="checkbox" id="landsat" onclick="mapLandSatTile();"/> Landsat 8 Imagery 2015 <br>
          <input type="checkbox" id="tileN9West" onclick="tileN9West();"/> Landsat 8 N9 Barat <br>
          <input type="checkbox" id="tileN9East" onclick="tileN9East();"/> Landsat 8 N9 Timur <br>
        </details>
        <details>
          <summary>Data Daerah Jelebu <br /></summary>
          <details>
            <summary>Jelebu - Getah<br /></summary>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(200);"/>==>Mukim Peradong0 <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(201);"/>==>Mukim Peradong1  <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(202);"/>==>Mukim Gelami Lemi  <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(203);"/>==>Mukim Hulu Kelawang <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(204);"/>==>Mukim Pertang <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(205);"/>==>Mukim Hulu Teriang <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(206);"/>==>Mukim Kenaboi <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(207);"/>==>Mukim Teriang Hilir <br>
          </details>

<!-- Jelebu - Getah <input type="checkbox" id="layer_04" onclick="toggleLayers(3);"/><br> -->
          Jelebu - Sawit <input type="checkbox" id="layer_03" onclick="toggleLayers(4);"/><br>
          <!-- Jelebu - Urban <input type="checkbox" id="layer_03" onclick="toggleLayers(5);"/><br> -->
          Jelebu - Clear Land <input type="checkbox" id="layer_03" onclick="toggleLayers(6);"/><br>
          Jelebu - Belukar/Hutan <input type="checkbox" id="layer_03" onclick="toggleLayers(7);"/><br>
        </details>
        <details>
          <summary>Data Daerah Seremban<br /></summary>

          <details>
            <summary>Seremban - Getah</summary>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(800);"/>==>Mukim Ampangan <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(801);"/>==>Mukim Bandar Seremban  <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(802);"/>==>Mukim Labu  <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(803);"/>==>Mukim Lenggeng <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(804);"/>==>Mukim Pantai <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(805);"/>==>Mukim Rantau <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(806);"/>==>Mukim Rasau <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(807);"/>==>Mukim Seremban <br>
            <input type="checkbox" id="layer_04" onclick="toggleLayers(808);"/>==>Mukim Setul <br>
          </details>

  <!-- Seremban - Getah <input type="checkbox" id="layer_03" onclick="toggleLayers(8);"/><br> -->
          Seremban - Sawit <input type="checkbox" id="layer_03" onclick="toggleLayers(9);"/><br>
          <!-- Seremban - Urban <input type="checkbox" id="layer_03" onclick="toggleLayers(10);"/><br> -->
          Seremban - Clear Land <input type="checkbox" id="layer_03" onclick="toggleLayers(11);"/><br>
          Seremban - Belukar/Hutan <input type="checkbox" id="layer_03" onclick="toggleLayers(12);"/><br>
        </details>
      </div>

      <div id="map-canvas"></div>
      <!-- <div id="status"></div> -->

      <a id="main-content"></a>
      <?php print $messages; ?>

    </section>

  </div>

</div>

<footer class="footer">
  <div id="footer-inner" class="container">
    <?php print render($page['footer']); ?>
  </div>
</footer>
