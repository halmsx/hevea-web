
<div class="ip-geoloc-map view-based-map">
  <?php echo ip_geoloc_output_map_multi_location($locations, $div_id, $map_options,
          $map_style, $marker_color, $visitor_marker, $center_option, $center_latlng, $visitor_location_gps);
  ?>
  <div id="status"></div>
</div>

